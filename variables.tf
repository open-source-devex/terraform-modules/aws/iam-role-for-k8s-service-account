##########################
# General module variables
##########################
variable "project" {
  description = "The name of the project"
  type        = string
}

variable "environment" {
  description = "The name of the environment"
  type        = string
}

variable "tags" {
  description = "Tags to be added to all reosurces that take it"
  type        = map(string)
  default     = {}
}

###########################
# Specific module variables
###########################
variable "create" {
  description = "Whether to create the IAM role reosurces and annotate the kubernetes service account"
  type        = bool
  default     = true
}

variable "create_iam_role" {
  description = "Whether to create the IAM role. Set to false when the annotation of the service account is all that is needed"
  type        = bool
  default     = true
}

variable "external_triggers" {
  description = "A map of triggers to be merged to the service account annotation (and rollout) triggers"
  type        = map(string)
  default     = null
}

variable "k8s_service_account_name" {
  description = "The name of the service account"
  type        = string
  default     = ""
}

variable "k8s_service_account_namespace" {
  description = "The name of the namspace where the service account lives, or when `service_account_name = \"\"` the namespace will scope the IAM role to all its accounts"
  type        = string
}

variable "k8s_fully_defined_resources" {
  description = "The fully qualified names of the resources (e.g \"daemonset.apps/aws-node\") that run under the service account (or within the namespace). This variable triggers a rollowut restart of the resources."
  type        = list(string)
  default     = []
}

variable "aws_iam_role_name" {
  description = "The name of the IAM role that the service account(s) can assume, leave empty to have a default name generated. Required when `create_iam_role = false`"
  type        = string
  default     = ""
}

variable "cluster_oidc_provider_url" {
  description = "The OpenID Connect provider URL of the cluster"
  type        = string
}

variable "aws_iam_role_policy_arns" {
  description = "A list of IAM policies to attatch to the role"
  type        = list(string)
  default     = []
}

##########################
# AWS variables
##########################
variable "aws_region" {
  description = "The AWS region where the cluster lives"
  type        = string
}

variable "aws_account_id" {
  description = "The id of the AWS account thay owns the cluster"
  type        = string
}

##############################
# Kubernetes cluster variables
##############################
variable "kubeconfig_file" {
  description = "The path to the kubeconfig file"
  type        = string
}
