locals {
  resource_name_suffix = "${var.project}-${var.environment}-${var.aws_region}"

  name = var.k8s_service_account_name != "" ? "${var.k8s_service_account_namespace}-${var.k8s_service_account_name}" : var.k8s_service_account_namespace

  created_iam_role_name = var.aws_iam_role_name != "" ? var.aws_iam_role_name : "${local.name}-${local.resource_name_suffix}"
  iam_role_name         = var.create_iam_role ? module.iam_assumable_role.this_iam_role_name : local.created_iam_role_name
  iam_role_arn          = var.create_iam_role ? module.iam_assumable_role.this_iam_role_arn : join("", data.aws_iam_role.existent_role.*.arn)

  have_service_account = var.k8s_service_account_name != ""
}
