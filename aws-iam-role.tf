locals {
  oidc_subject = "system:serviceaccount:${var.k8s_service_account_namespace}:${local.have_service_account ? var.k8s_service_account_name : "*"}"
}

module "iam_assumable_role" {
  source = "terraform-aws-modules/iam/aws//modules/iam-assumable-role-with-oidc"
  version = "~> 2.22"

  create_role = var.create && var.create_iam_role

  provider_url = var.cluster_oidc_provider_url

  role_name        = local.created_iam_role_name
  role_policy_arns = var.aws_iam_role_policy_arns

  oidc_fully_qualified_subjects = compact([local.have_service_account ? local.oidc_subject : ""])
  oidc_subjects_with_wildcards  = compact([local.have_service_account ? "" : local.oidc_subject])

  tags = var.tags
}

data "aws_iam_role" "existent_role" {
  count = var.create && (!var.create_iam_role) ? 1 : 0

  name = var.aws_iam_role_name
}
