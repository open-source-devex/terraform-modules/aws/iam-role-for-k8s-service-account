# aws-iam-role-for-service-account

Terraform module to create an AWS IAM role, the policy that goes with it and annotate a service account in order for related pods to assume the role.

Reference: https://docs.aws.amazon.com/en_pv/eks/latest/userguide/iam-roles-for-service-accounts.html

##  Notes

Knowing when to trigger a SA annotation and roll-out restart cen be tricky. Let us kook at it from the perspective of specific scenarios.

### Scenario 1 - service account is being created in the same terraform run as this module is being instantiated

In this scenario it could happen that terraform does not identify any implicit dependency between the resource that creates the SA and the resources in this module that annotate it.
In that case, we need to pipe an output from the resource that creates the service account into this module's `var.service_account_created_trigger`.
That way, this module can create an artificial dependency with a `null_data_source` to make sure annotation waits for the creation of the SA.

### Scenario 2 - service account annotation is removed outside of terraform

If it happens that the annotation is lost (e.g. the SA is recreated) outside of terraform's knowledge, then this module should re-trigger the annotation.
To that end the module includes a script that queries the cluster using kubectl and reads the annotations on the SA.
If the annotation is not present the module annotates it.
Because we are doing this via triggers, it will happen, in this scenario, marking a trigger as "annotation missing" the next time terraform runs and the annotation is present, the trigger is updated to "annotation present".  
That means that the module will once again trigger the annotation and roll-out restart once more, for every time the SA goes missing.

### Scenario 3 - The role with which we annotated the service account changes

Because the role ARN is a trigger to the resources that annotate and do the roll-out restart, when that changes the module triggers again and adds the annotation.

### Scenario 4 - We need to trigger the module for some other reason

The module provides a `var.external_trigger` that takes a `map(string)` and merges that with the triggers for the resources in this module.
That means that changing any value in that map will re-trigger the annotation.
