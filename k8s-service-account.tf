locals {
  k8s_sa_annotation = ["eks.amazonaws.com/role-arn", local.iam_role_arn]

  external_trigger = var.external_triggers != null ? var.external_triggers : {}

  # check if annotation is present in the SA
  current_annotations        = try(data.external.query_service_account[0].result, {})
  # Setting the trigger to a constant value makes the annotation trigger once more than needed
  # but makes sure that the annotation is added if missing
  missing_annotation_trigger = try(local.current_annotations["eks.amazonaws.com/role-arn"], "") != local.iam_role_arn ? "annotation-missing-${timestamp()}" : "annotation-present"
}

resource "null_resource" "annotate_service_account" {
  count = var.create && local.have_service_account ? 1 : 0

  provisioner "local-exec" {
    when = create

    environment = {
      KUBECONFIG = var.kubeconfig_file
    }

    command = "kubectl annotate serviceaccount -n ${var.k8s_service_account_namespace} ${var.k8s_service_account_name} ${join("=", local.k8s_sa_annotation)} --overwrite"
  }

  triggers = merge(local.external_trigger, {
    k8s_service_account_namespace = var.k8s_service_account_namespace
    k8s_service_account_name      = var.k8s_service_account_name
    iam_role                      = module.iam_assumable_role.this_iam_role_arn
    oidc_subject                  = local.oidc_subject
    kubeconfig_file               = sha1(var.kubeconfig_file)
    missing_annotation_trigger    = local.missing_annotation_trigger
  })
}

resource "null_resource" "rollout_restart" {
  count = var.create ? length(var.k8s_fully_defined_resources) : 0

  provisioner "local-exec" {
    when = create

    environment = {
      KUBECONFIG = var.kubeconfig_file
    }

    command = "kubectl rollout restart -n ${var.k8s_service_account_namespace} ${var.k8s_fully_defined_resources[count.index]}"
  }

  triggers = merge(local.external_trigger, {
    k8s_service_account_namespace = var.k8s_service_account_namespace
    k8s_service_account_name      = var.k8s_service_account_name
    iam_role                      = module.iam_assumable_role.this_iam_role_arn
    oidc_subject                  = local.oidc_subject
    kubeconfig_file               = sha1(var.kubeconfig_file)
    missing_annotation_trigger    = local.missing_annotation_trigger
  })

  depends_on = [null_resource.annotate_service_account]
}

data "external" "query_service_account" {
  count = var.create && local.have_service_account ? 1 : 0

  program = ["${path.module}/scripts/query-sa-annotations.sh"]

  query = {
    namespace       = var.k8s_service_account_namespace
    service_account = var.k8s_service_account_name
    kubeconfig      = var.kubeconfig_file
  }
}
