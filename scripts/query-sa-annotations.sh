#!/usr/bin/env bash


# Extract "namespace" and "service_account" arguments from the input into
# NAMESPACE and SERVICE_ACCOUNT shell variables.
# jq will ensure that the values are properly quoted
# and escaped for consumption by the shell.
eval "$(jq -r '@sh "NAMESPACE=\(.namespace) SERVICE_ACCOUNT=\(.service_account) KUBECONFIG=\(.kubeconfig)"')"

kubectl_output=$( kubectl get serviceaccount ${SERVICE_ACCOUNT} -n ${NAMESPACE} --kubeconfig ${KUBECONFIG} -ojson )

if [[ "${kubectl_output}" != "" ]]; then
	metadata=$( echo -n "${kubectl_output}" | jq '.metadata' )
	if [[ "${metadata}" != "" ]]; then
		annotations=$( echo -n "${kubectl_output}" | jq '.metadata .annotations' )
		if [[ "${annotations}" != "" ]]; then
			echo "${annotations}" | jq
			exit 0
		fi
	fi
fi

echo '{}'
