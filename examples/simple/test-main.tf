terraform {
  backend "remote" {
    organization = "open-source-devex"
    workspaces {
      name = "terraform-modules-k8s-aws-iam-role-for-service-account"
    }
  }
}

provider "aws" {
  region = "eu-west-1"
}

data "aws_caller_identity" "current" {}
data "aws_region" "current" {}

variable "kubeconfig_file" {}

variable "cluster_oidc_provider_url" {}

variable "external_iam_role_name" {}

module "aws_iam_role_for_k8s_sa_wildcard" {
  source = "../../"

  project         = "project"
  environment     = "environment"
  aws_account_id  = data.aws_caller_identity.current.account_id
  aws_region      = data.aws_region.current.name
  kubeconfig_file = var.kubeconfig_file

  cluster_oidc_provider_url     = var.cluster_oidc_provider_url
  k8s_service_account_namespace = "default"
  k8s_fully_defined_resources   = []
}

module "aws_iam_role_for_k8s_sa_named" {
  source = "../../"

  project         = "project"
  environment     = "environment"
  aws_account_id  = data.aws_caller_identity.current.account_id
  aws_region      = data.aws_region.current.name
  kubeconfig_file = var.kubeconfig_file

  cluster_oidc_provider_url     = var.cluster_oidc_provider_url
  k8s_service_account_namespace = "kube-system"
  k8s_service_account_name      = "aws-node"
  k8s_fully_defined_resources   = ["daemonset.apps/aws-node"]
}

module "aws_iam_role_for_k8s_sa_external_role" {
  source = "../../"

  project         = "project"
  environment     = "environment"
  aws_account_id  = data.aws_caller_identity.current.account_id
  aws_region      = data.aws_region.current.name
  kubeconfig_file = var.kubeconfig_file

  create_iam_role   = false
  aws_iam_role_name = var.external_iam_role_name

  cluster_oidc_provider_url     = var.cluster_oidc_provider_url
  k8s_service_account_namespace = "default"
  k8s_service_account_name      = "default"
}
